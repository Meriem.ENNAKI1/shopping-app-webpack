import React from "react";
import "./App.css";
import Article from "./components/Article";
import { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "font-awesome/css/font-awesome.min.css";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Article />
      </div>
    );
  }
}

export default App;
