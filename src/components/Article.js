import React, { Component } from "react";

export default class Article extends Component {
  state = {
    articles: [
      {
        id: 1,
        nom: "Lait",
        quantite: 0,
        deleted: false
      },
      {
        id: 2,
        nom: "Sucre",
        quantite: 0,
        deleted: false
      }
    ]
  };

  addArticle = index => {
    const newArticles = [...this.state.articles];
    const item = newArticles[index].quantite;
    newArticles[index].quantite = item + 1;
    this.setState({ articles: newArticles });
  };
  deletArticle = index => {
    const newArticles = [...this.state.articles];
    newArticles[index].deleted = true;
    this.setState({ articles: newArticles });
  };

  getTotal() {
    const { articles } = this.state;
    let total = 0;
    articles.map(article => (total = total + article.quantite));
    return total;
  }
  render() {
    return (
      <div>
        <table>
          <tr>
            <th>Nom</th>
            <th>Quantite</th>
            <th>Actions</th>
          </tr>
          {this.state.articles.map(
            (article, index) =>
              !article.deleted && (
                <tr key={index}>
                  <td>{article.nom}</td>
                  <td>{article.quantite}</td>
                  <button
                    className="btn btn-secondary btn-sm"
                    onClick={() => this.addArticle(index)}
                  >
                    <i className="fa fa-plus" />
                  </button>

                  <button
                    className="btn btn-secondary btn-sm"
                    onClick={() => this.deletArticle(index)}
                  >
                    Remove
                  </button>
                </tr>
              )
          )}
          <span>Total</span>
          {this.getTotal()}
        </table>
      </div>
    );
  }
}
